# README #

The web application can be viewed at http://resa.projects.webmy.me/.
The UI is based on a free bootstrap template (agency).
The front-end is using AngularJS, MomentJS and Angular-Strap.
The back-end is MYSQL, PHP (Slim Framework, RedbeanPHP).

### What is this repository for? ###

This was a coding exercise and here is how the problem was presented:

Please provide an application which will perform the following:


* For a given restaurant from a list of known restaurants, select a time slot for making a reservation for a given party size

* If the restaurant is booked at the selected time, show a message that no reservation can be done

* If the reservation can be made, show the reservation confirmation, with time slot, restaurant name, and party size


Feel free to use a database or not to persist data.