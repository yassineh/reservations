#!/usr/bin/env sh
SRC_DIR="`pwd`"
cd "`dirname "$0"`"
cd '../malkusch/php-autoloader/bin'
BIN_TARGET="`pwd`/autoloader-build.php"
cd "$SRC_DIR"
"$BIN_TARGET" "$@"
