reservationsApp.controller('RestaurantsListCtrl',
    function ($scope, RestaurantServiceImpl) {

        $scope.restaurants = [];
        $scope.restaurantService = RestaurantServiceImpl;

        if (!$scope.restaurantService.loaded && !RestaurantServiceImpl.isLoading)
            RestaurantServiceImpl.getRestaurants();

});
