reservationsApp.controller('ReservationCtrl',
    function ($scope, RestaurantServiceImpl, ReservationServiceImpl) {
        $scope.restaurantService = RestaurantServiceImpl;
        $scope.reservationService = ReservationServiceImpl;



        $scope.range = function(start, end) {
            var result = [];
            for (var i = start; i <= end; i++) {
                result.push(i);
            }
            return result;
        };

        var initReservation = function(){
            $scope.reservation = {
                'id': null,
                'restaurant':null,
                'dateStart':null,
                'seats':1,
                'clientName':''
            };

            $scope.reservationComplete = false;
            $scope.formValidation = {};
            $scope.errorMessage = '';
        };

        if (!RestaurantServiceImpl.loaded && !RestaurantServiceImpl.isLoading)
            RestaurantServiceImpl.getRestaurants();

        var validateForm = function(){
            $scope.formValidation = {};
            if (!$scope.reservation.clientName)
                $scope.formValidation.clientName = true;
            if (!$scope.reservation.restaurant)
                $scope.formValidation.restaurant = true;
            if (!$scope.reservation.dateStart)
                $scope.formValidation.dateTime = true;
            $scope.formValidation.valid = (Object.keys($scope.formValidation).length == 0);
        };
        $scope.makeReservation = function(){
            validateForm();
            $scope.errorMessage = '';
            if (!$scope.formValidation.valid)
                return;
            ReservationServiceImpl.makeReservation($scope.reservation)
                .success(function(data){
                    $scope.reservationComplete = true;
                })
                .error(function(data){
                    if (data.data.message)
                        $scope.errorMessage = data.data.message;
                });
        };

        $scope.getRestaurantName = function(){
            if (!$scope.reservation.restaurant)
                return '';
            var found = '';
            RestaurantServiceImpl.restaurants.forEach(function(elt){
                if (parseInt(elt.id) == parseInt($scope.reservation.restaurant)){
                    found = elt.name;
                    return;
                }
            });
            return found;
        };

        $scope.getReservationDate = function(){
            if (!$scope.reservation.dateStart)
                return '';
            return moment($scope.reservation.dateStart).format('MMMM Do YYYY, h:mm:ss a');
        };

        $scope.anotherReservation = function(){
            initReservation();
        };

        initReservation();
    });
