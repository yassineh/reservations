var reservationsApp = angular.module('reservationsApp', [
    'RestaurantService',
    'ReservationService',
    'ngAnimate',
    'ngSanitize',
    'mgcrea.ngStrap'
]);
