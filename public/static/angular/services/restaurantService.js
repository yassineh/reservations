var RestaurantService = angular.module('RestaurantService', []);

RestaurantService.factory('RestaurantServiceDataOp', ['$http', function ($http) {

    var urlBase = '/api';
    var RestaurantServiceDataOp = {};

    RestaurantServiceDataOp.getRestaurants = function () {
        return $http.get(urlBase+'/restaurants');
    };

    return RestaurantServiceDataOp;

}]);

RestaurantService.service('RestaurantServiceImpl', ['RestaurantServiceDataOp', function (RestaurantServiceDataOp) {

    var thisService = this;
    this.restaurants = [];
    this.isLoading = false;
    this.loaded = false;

    this.getRestaurants = function(){
        thisService.isLoading = true;
        RestaurantServiceDataOp.getRestaurants()
            .success(function (data) {
                thisService.restaurants = data.data;
                thisService.loaded = true;
            })
            .error(function (error) {
                console.log('Unable to load data: ' + error.message);
            })
            .finally(function(){
                thisService.isLoading = false;
            });
    };
}]);