var ReservationService = angular.module('ReservationService', []);

ReservationService.factory('ReservationServiceDataOp', ['$http', function ($http) {

    var urlBase = '/api';
    var ReservationServiceDataOp = {};

    ReservationServiceDataOp.makeReservation = function (data) {
        return $http.post(urlBase+'/reservations', data);
    };

    return ReservationServiceDataOp;

}]);

ReservationService.service('ReservationServiceImpl', ['ReservationServiceDataOp', function (ReservationServiceDataOp) {

    var thisService = this;
    this.restaurants = [];
    this.isLoading = false;
    this.loaded = false;

    this.makeReservation = function(data){
        thisService.isLoading = true;
        var promise = ReservationServiceDataOp.makeReservation(data);

        promise.success(function (data) {
            thisService.loaded = true;
        })
        .error(function (error) {
            console.log('Unable to load data: ' + error.message);
        })
        .finally(function(){
            thisService.isLoading = false;
        });
        return promise;
    };
}]);