<?php

namespace app\Controllers\Api;


use app\Controllers\BaseController;
use app\Services\ReservationService;
use Slim\Slim;

class Reservations extends BaseController
{
    public function save(){
        $postedData = $this->getJsonPostData();
        $service = new ReservationService();
        try {
            $this->jsonDataResponse(array('reservationId'    =>  $service->save($postedData)));
        } catch (\Exception $e) {
            $this->jsonExceptionResponse($e);
        }
    }
}