<?php

namespace app\Controllers\Api;

use app\Controllers\BaseController;
use app\Services\RestaurantService;

class Restaurants extends BaseController
{
    public function get()
    {
        $restaurantService = new RestaurantService();
        $result = $restaurantService->getAll();
        $this->jsonDataResponse($result);
    }
}