<?php

namespace app\Controllers;

use Slim\Slim;

class Homepage extends BaseController
{
    public function get(){
        $app = Slim::getInstance();
        $twig = $this->getTwig();
        $this->htmlResponse($twig->render('base.html.twig', array('title' => 'Reservations')));
    }
}