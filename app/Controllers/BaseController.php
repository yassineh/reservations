<?php
/**
 * Created by IntelliJ IDEA.
 * User: yassinehaddioui
 * Date: 9/20/15
 * Time: 12:42 PM
 */

namespace app\Controllers;

use Twig_Loader_Filesystem;
use Twig_Environment;
use Slim\Slim;
use \Exception;

class BaseController
{

    public function __construct()
    {
    }

    function getTwig(){
        $app =Slim::getInstance();
        $loader = new Twig_Loader_Filesystem($app->config('templates.path'));
        return new Twig_Environment($loader, array());
    }

    protected function getJsonPostData(){
        $app = Slim::getInstance();
        $postedData = $app->request->getBody();
        if (!empty($postedData))
            return json_decode($postedData, true);
        return array();
    }

    /**
     * @param Exception $exception
     * @param int $statusCode
     */
    protected function jsonExceptionResponse($exception, $statusCode = 500){
        $response = array(
            'message' =>  $exception->getMessage(),
            'code'    =>  $exception->getCode());
        $this->jsonDataResponse($response, $statusCode);
    }

    /**
     * @param mixed $data
     * @param int $statusCode
     */

    protected function jsonDataResponse($data, $statusCode = 0){
        $app = Slim::getInstance();
        if ($statusCode)
            $app->response->setStatus($statusCode);
        $app->response->headers->set('Content-Type', 'application/json');
        $app->response->setBody(json_encode(array('data'    =>  $data)));
    }

    /**
     * @param string $html
     * @param int $statusCode
     */

    protected function htmlResponse($html, $statusCode = 0){
        $app = Slim::getInstance();
        $app->response->setBody($html);
        if ($statusCode)
            $app->response->setStatus($statusCode);
    }
}