<?php
/* Class Auto loaders */
require '../vendor/autoload.php';
require '../vendor/malkusch/php-autoloader/autoloader.php';

/* DBAL */
require 'lib/rb.php';

R::setup('mysql:host=127.0.0.1;dbname=reservations',
    'adminuser', 'adminuser', true);

use Slim\Slim;
global $app;



$app = new Slim(array(
    'debug' => true,
    'templates.path' => '../views/'
));

require 'routes.php';