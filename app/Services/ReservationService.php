<?php
/**
 * Created by IntelliJ IDEA.
 * User: yassinehaddioui
 * Date: 9/20/15
 * Time: 4:48 PM
 */

namespace app\Services;
use R;

class ReservationService
{
    const RESERVATION_DURATION = 3600;

    public function save($postData){
        $this->validate($postData);
        $newReservation = R::dispense('reservation');
        $newReservation->import($postData);
        $this->checkAvailability($newReservation);
        $saved = R::store($newReservation);
        return $saved;
    }

    private function validate($data){
        if (empty($data['restaurant']) || empty($data['dateStart']) || empty($data['clientName']))
            throw new \InvalidArgumentException('Missing parameter.', 200);
    }

    private function checkAvailability($newReservation){
        $slots = R::findAll( 'reservation', ' restaurant = :restaurant AND date_start > DATE_SUB(:dateStart, INTERVAL 1 HOUR) AND date_start < DATE_ADD(:dateStart, INTERVAL 1 HOUR)', array(
            ':restaurant'   =>  (int)   $newReservation->restaurant,
            ':dateStart'   =>  $newReservation->dateStart
        ));
        if (empty($slots))
            return true;
        $restaurant = R::load('restaurant', $newReservation->restaurant);
        $remainingSeats = (int) $restaurant->capacity;
        foreach ($slots as $slot){
            $remainingSeats -= (int) $slot->seats;
        }
        if ($remainingSeats < $newReservation->seats){
            throw new \Exception('Not enough available seats', 500);
        }
        return true;
    }
}