<?php

namespace app\Services;
use R;

class RestaurantService
{
    public function getAll(){
        $restaurants = R::getAll('SELECT * FROM restaurant');
        return $restaurants;
    }

}