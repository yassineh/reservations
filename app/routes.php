<?php

global $app;
use \app\Controllers\Homepage;
use \app\Controllers\Api\Restaurants;
use \app\Controllers\Api\Reservations;

$app->get('/', function () {
    $homepage = new Homepage();
    $homepage->get();
});

$app->get('/api/restaurants', function () {
    $restaurants = new Restaurants();
    $restaurants->get();
});

$app->post('/api/reservations', function () {
    $reservations = new Reservations();
    $reservations->save();
});